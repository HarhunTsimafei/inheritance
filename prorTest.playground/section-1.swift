// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


//let a = (x: (5), y: (7))
let b = 3
let name = "ROma"

switch name {
//case (4...7, 7): println("1")
//case (4...7, 5): println("2")
//case _ where b == 3: println("3")
case _ where name.hasPrefix("z"): println("3")
case let a where a.hasPrefix("R"): println("4")

    
default: break

}

let a = 3...5
println("\(a)")
