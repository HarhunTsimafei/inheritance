//
//  THPerson.cpp
//  labRab8_C++
//
//  Created by Timofei Harhun on 12.02.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#include "THPerson.h"
#include <iostream>

THPerson* THPerson::begin=NULL;

using std::endl;
using std::cout;
using std::cin;

void THPerson::showPeron(){
    cout<<"My name "<<getName()<<endl;
    cout<<"My last name "<<getLastName()<<endl;
    showDateOfBirth();
    cout<<endl;
    cout<<"My salary "<<getSalary()<<endl;			
}

THPerson::THPerson(){
    this->address=NULL;
    
    if(begin==NULL) {
        begin=this;
    } else {
        THPerson* a = begin;
        for (; ; ) {
            if (a->address!=NULL) {
                THPerson* b = a->address;
                a=b;
            } else {
                a->address=this;
                break;
            }
        }
    }

}

void THPerson::addOblInList(void){
    THPerson* a=begin;
    if(begin==NULL) {
        begin=this;
        this->address=NULL;
    } else {
    for (; ; ) {
        if (a->address!=NULL) {
            THPerson* b = a->address;
            a=b;
        } else {
            a->address=this;
            this->address=NULL;
            break;
        }
        
    }}
}

void THPerson::show(){
    THPerson* a=begin;
    for (int i=1; ; i++) {
        cout<<"Обьект номер "<<i<<" : "<< a<<endl;
        if (begin!=NULL && a->address!=NULL) {
            THPerson* b = a->address;
            a=b;
        } else {
            break;
        }
        
    }
};

void THPerson::setName(string name){
    this->name=name;
}

void THPerson::setLastName(string lastName){
    this->lastName=lastName;
}

void THPerson::setBirthDateOfBirth(int day, int mounth, int year){
    THBirthday date;
    date.setDay(day);
    date.setMounth(mounth);
    date.setYear(year);
    
    this->dateOfBirth=date;
}


void THPerson::setSalary(int sal){
    this->salary=sal;
}

string THPerson::getName(){
    return this->name;
}

string THPerson::getLastName(){
    return this->lastName;
}

THBirthday THPerson::getdateOfBirth(){
    return this->dateOfBirth;
}

void THPerson::showDateOfBirth(){
    this->dateOfBirth.showBirthday();
}
;

int THPerson::getSalary(){
    return this->salary;
};