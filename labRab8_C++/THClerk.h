

#ifndef __labRab8_C____THClerk__
#define __labRab8_C____THClerk__

#include <stdio.h>
#include "THPerson.h"

class THClerk: public THPerson {
        int qualification;
public:
    

    virtual void showPeron();
    void setQualification(int);
    int getQualification();
};

#endif
