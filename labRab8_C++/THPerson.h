#ifndef __labRab8_C____THPerson__
#define __labRab8_C____THPerson__

#include <stdio.h>
#include <string>
#include "THBirthday.h"

using std::string;

class THPerson {
    
    string name;
    string lastName;
    
    int salary;
    
    THBirthday dateOfBirth;
    
protected:
    static THPerson* begin;
    THPerson* address;
    
public:
    
    THPerson();
    
    void setName(string);
    void setLastName(string);
    void setBirthDateOfBirth(int, int, int);
    void setSalary(int);
    virtual void showPeron(void);
    
    
    int getSalary();
    string getName();
    string getLastName();
    void showDateOfBirth();
    THBirthday getdateOfBirth();

    static void show();
    void addOblInList(void);
};



#endif
