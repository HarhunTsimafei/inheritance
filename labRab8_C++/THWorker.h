#ifndef __labRab8_C____THWoker__
#define __labRab8_C____THWoker__

#include <stdio.h>
#include "THPerson.h"

class THWorker: public THPerson {
    int seniority;
public:
    void setSeniority(int);
    int getSeniority(void);
};

#endif
