//
//  THEngineer.cpp
//  labRab8_C++
//
//  Created by Timofei Harhun on 12.02.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#include "THEngineer.h"
#include <iostream>
using std::cout;
using std::endl;

void THEngineer::setProfession(profession prof){
    this->skill=prof;
}
profession THEngineer::getProfession(void){
    return this->skill;
}

void THEngineer::showPeron(){
    cout<<"My name "<<getName()<<endl;
    cout<<"My last name "<<getLastName()<<endl;
    showDateOfBirth();
    cout<<endl;
    cout<<"My salary "<<getSalary()<<endl;
    cout<<"My qualification "<<getQualification()<<endl;
}