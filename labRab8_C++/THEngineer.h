

#ifndef __labRab8_C____THEngineer__
#define __labRab8_C____THEngineer__

#include <stdio.h>
#include "THClerk.h"


enum profession {
    coder,
    systematic,
    designer,
    webProgramer
};

class THEngineer: public THClerk {
    profession skill;
public:
    void showPeron();
    void setProfession(profession);
    profession getProfession(void);
};

#endif
