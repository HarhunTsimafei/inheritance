

#include <iostream>
#include "THClerk.h"
#include "THWorker.h"
#include "THEngineer.h"

using namespace std;
int main() {
    
    THWorker workerFrist;
    workerFrist.setName("Pavel");
    workerFrist.setLastName("Harhun");
    workerFrist.setBirthDateOfBirth(1, 11, 1964);
    workerFrist.setSalary(10000);
    workerFrist.setSeniority(10);
    
    THWorker workerSecond;
    workerSecond.setName("Roman");
    workerSecond.setLastName("Genius");
    workerSecond.setBirthDateOfBirth(3, 2, 1973);
    workerSecond.setSalary(20000);
    workerSecond.setSeniority(8);
    
    THClerk clercFirst;
    clercFirst.setName("Kirill");
    clercFirst.setLastName("Korobeiko");
    clercFirst.setBirthDateOfBirth(2, 5, 1995);
    clercFirst.setSalary(15000);
    clercFirst.setQualification(2);
    
    THEngineer engineerOne;
    engineerOne.setName("Filip");
    engineerOne.setLastName("Rovot");
    engineerOne.setBirthDateOfBirth(6, 10, 1983);
    engineerOne.setSalary(20000);
    engineerOne.setQualification(5);
    engineerOne.setProfession(designer);
    
    THEngineer engineerTwo;
    engineerTwo.setName("Tsimafei");
    engineerTwo.setLastName("Harhun");
    engineerTwo.setBirthDateOfBirth(1, 3, 1995);
    engineerTwo.setSalary(25000);
    engineerTwo.setQualification(5);
    engineerTwo.setProfession(coder);
    
    
     workerFrist.show();
    return 0;
}
