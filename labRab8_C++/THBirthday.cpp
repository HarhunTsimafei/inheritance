//
//  THBirthday.cpp
//  labRab8_C++
//
//  Created by Timofei Harhun on 12.02.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#include "THBirthday.h"
#include "iostream"


using std::cout;

void THBirthday::setDay(int day){
    if (1<=day && day<=31){
        this->day=day;
    } else {
        cout << "Day was not set" << std::endl;
    }
}

void THBirthday::setMounth(int mounth){
    if (1<=mounth && mounth<=12){
        this->mounth=mounth;
    } else {
        cout << "Mounth was not set" << std::endl;
    }
}

void THBirthday::setYear(int year){
    if (1900<=year && year<=2015){
        this->year=year;
    } else {
        cout << "Mounth was not set" << std::endl;
    }
}

void THBirthday::showBirthday(){
    cout<<this->day<<" ";
    switch (this->mounth) {
        case 1:
            cout << "января ";
            break;
        case 2:
            cout << "февраля ";
            break;
        case 3:
            cout << "марта ";
            break;
        case 4:
            cout << "апреля ";
            break;
        case 5:
            cout << "мая ";
            break;
        case 6:
            cout << "июня ";
            break;
        case 7:
            cout << "июля ";
            break;
        case 8:
            cout << "августа ";
            break;
        case 9:
            cout << "сентября ";
            break;
        case 10:
            cout << "окрября ";
            break;
        case 11:
            cout << "ноября ";
            break;
        case 12:
            cout << "декабря ";
            break;
            
        default:
            break;
    }
    cout << this->year;
}

THBirthday& THBirthday::operator=(const THBirthday & date){
    this->day=date.day;
    this->mounth=date.mounth;
    this->year=date.year;
    return *this;
}
